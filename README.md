# Simple Note Ami 01

注：Amiさんへ〜これ本来の GitLab の使い方ではない一時連絡用、そのうち削除。必要ならリンクなどは保存して下さい。

## Linux のオススメ OS 

元になったOSからの派生をDistributionと呼び、Debian系の Ubuntu か更にその派生の ElementaryOS が良いと思う。

 - [Ubuntu本家](https://ubuntu.com/) 現在はver.20出たばかり、なおver.18以降ならまぁだいたい同じ

 - [Ubuntu日本語](https://www.ubuntulinux.jp/japanese) （本家より少し古く説明がヘタ…ま、こんなもんでしょうｗ）

 - [Elementary OS](https://elementary.io/ja/) 推し〜個人的に注目、だが試してない（次回ぜひ使いたい）
    → [Supportページ](https://elementary.io/ja/support) これUbuntuより良く出来てる

Ubuntu か Elementary OS か、どちらも"Debian"系です。
どちらを使うかは、ネット上の情報を見て決めたら良いと思う。

- [【目的別】初心者におすすめのLinuxディストリビューション7選](https://eng-entrance.com/linux_beginner_distribution) これ明快ですね

- [elementary OS 5.1 Hera がリリース](https://link.medium.com/7g5Qc36825) 他にもたくさん出てくるでしょう

ただ、シェアは全 Linux 中で Ubuntu が圧倒的。ネット上の情報も当然たくさんある…けどゴミ情報も多い。見極める目が必要。ネット上は他人のサイトからの剽窃コピーゴミばっかり（泣）場合やモノによるけど、エンジニア的な視点で見ると比率はこんな感じ…？

**ゴミ：少し参考：まぁまぁ：良い  →  ４：３：２：１**

あれま〜 :smile:  :+1: 

## Linux と Windows のデュアルブート

[WindowsにUbuntuをインストールするのは気持ち良いゾイ!!~導入編~](https://qiita.com/Takumi_Kaibara/items/4132e552a615a050a841) （一部少し古いがまだ通用する）

[Windows 10とUbuntu 18.04 デュアルブートする方法](https://www.pc-koubou.jp/magazine/35542)

注：Linuxの日本語入力は"Mozc"は「[Google日本語入力](https://www.google.co.jp/ime/)」の派生バージョン〜僕のMacも日本語変換はGoogle使いｗ

それから、主戦場は動画＝YouTubeに移りつつある。ざっとしかみてないけど、以下とか。良く出来てるなぁ。

[Ubuntu 20.04 For Windows Users](https://youtu.be/UBCE-ZH_xLA)
３分１秒辺り [起動時にキー長押し](https://youtu.be/UBCE-ZH_xLA?t=181) でBIOS画面で切替えがキモ！！このポイント外すと永久に分かんない
	 → 次のどれか `F1`  `F5`  `F6`  `F10`  `esc`  `del` キー長押しでBIOS画面に入る

[How to Dual Boot Ubuntu 20.04 LTS and Windows 10 [ 2020 ]](https://youtu.be/-iSAyiicyQY)
これも [起動時にキー長押し](https://youtu.be/-iSAyiicyQY?t=283) でWin起動前にBIOS画面で切替え。このキーは製造メーカーにより異なる！注意！！

日本語情報は、少し英語わかるやつが海外の情報見て知ったかぶりな劣化コピーも多く時間のムダ。

## VSCode について

もちろん開発ツールは揃ってる。VSCode当然。なお未確認だけど Ubuntu / Elementary どちらもアプリストアがあって、そこからDL可能と思われる。
現時点では完全にVSCode一択になってるので、Eclipseとか、若い子だと名前さえ知らなかったりするかも。

[Visual Studio Code](https://code.visualstudio.com/Download) これは "debian" の方を選択、Ubuntu, Elementary 共通

MacやWinと違い、コマンドラインでインストールとか、生のままのコマンドを打つ的なものが出てくる場合はあるけど、読めばわかると思う。
その部分もElementaryのほうがユーザーフレンドリーな感じがする。

## 開発モデルなど

日本ゼネコン模倣のブラック業界的SIer（エスアイ屋 System Integrater）は、かなり軽蔑しつつガン無視なので基本的に英語で情報検索。
僕のMacも第１言語は英語で「日本語が超上手い米人」がイメージ。
日本国内から検索するとGoogleが検知するので、日本語情報がまず出てくる…エンジニア的にウザい。ラーメン屋を探す場合は良いんだけど。

 - かつては大げさにシステム計画し、IT土方人足を配置、孫請け〜ひ孫請け〜玄孫請け上等で丸投げ → Waterfall 開発モデル

 - 現在は Git で共有しながら短いサイクルで計画とテストと改良を繰り返して大きくしていくスピードが命 → DevOps 開発モデル

できるエンジニアなら個人で世界相手に稼ぐし、そのため英語は必須。日本語の説明が無いとわからないとか、なに寝ぼけたこと言ってんの？と思う。
日本ドメスティックなIT土方人足として消耗したい？のかと思う。教材なんて Googleで Programming, Online, Free で検索すればいくらでもある。

でも、基本的に日本人は几帳面で優れてると思う（じつは）…だから信頼に足る人の説明は参考になるし、慣れてるせいか表意文字の漢字混じりは読みやすい。
アルファベット記述のコード部分と明確に分かれるし。知合いの米国育ちスタンフォード卒な日本人も「漢字上手く発音読めないけど、本読む時は日本語のほうが速い」と言ってた。

## GitHubとGitLab、それにSlack

私がもしエンジニア採用するなら、まず [GitHub](https://github.com) か [GitLab](https://gitlab.com) のアカウントをたずねる。それ持って無いとか論外（笑）「あぁぁ…後日またね〜」でさようなら。
ちょと Git というものが最初は取っ付き辛いの知ってます… がしかし、今どきのエンジニアなら"超"常識。

[Tomomi Imura @girlie_mac](https://twitter.com/girlie_mac) 井村さんの [Gitイラスト解説](https://twitter.com/girlie_mac/status/1044044900755369984) なかなか良いでしょう？
これは厳密には「Git」についての解説で、Git共有サイトがGitHubやGitLabです。共同で一つのものを仕上げる、というプロセスの話で難しいことじゃない。
その気なら、学級新聞とかシンフォニーの作編曲とかにも使えるはず。

ちなみに彼女はエンジニア用SNSの [Slack](https://slack.com) のエバンジェリストで、7割位アメリカ人な素敵な人 Girlie Mac 名前センス良いなぁ
このSlack は一般のSNSと少し違うけど、こちらもエンジニア必須な感じです。そういえば先日、SlackがJR総武線内の車内広告TVでCM流してたのにはビックリした！

 [GitHub](https://github.com) と [Slack](https://slack.com) ぜひチェックしてみて下さい。人に教えるにしてもエンジニアとしても必須。
 
うーん GitHub や Slack の話題が普通に出てこないエンジニア教室（職業訓練所のもそんな空気？）って何なのでしょうか。

以下の記事など読むにつけ、日本人エンジニアやデザイン業界、日本企業ぽい話とか基本的に僕の視野に入ってません。
たくさんブックマーク保存してあるけど、１つどうぞ。

[なぜ日本ではデザイナーの地位が上がらないのか？](https://blog.btrax.com/jp/designers-in-japan/)

## ダメなほうの見本

ちょと検索したら上位に提示してきたこれ、アクセス稼ぎのサイト。こんなんばっかりでグッタリ…。WindowsにさらにUbuntuを上乗せするやつ。
まあ悪い見本？を一応知っておく意味はある（パラドックスｗ）と思います。

[WSL(Windows10)にLinuxのUbuntuインストール](https://reffect.co.jp/windows/wsl-windows-ubuntu-install)

他にSEOだけ頑張ってもはや嘘が書かれている「侍エンジニア」とかいう学校、何なのこれ。

[【時事問題】どうやら侍エンジニア塾が火にガソリンを注いでる模様【ある意味読書感想文】](https://sugaryo1224.hatenablog.com/entry/2018/10/23/195535)

## おしまい

キリが無いから、このへんで。またしばらくしたら、お会いしましょう。

今回はGitLabの方にノート置きました。GitHubの方がシェアも多くメイン使いだけど、あっちでやると「太田さん何してんの？」になるのでｗ。

サイト公開する仕組み [GitLab Pages](https://gitlab.com/pages) 使うとさらに大げさなのでやめてます。
初めてGitLab でMarkdown を書いてみたけど、うぅむやっぱり GitHub のほうが良い感じ。日本語だとさらに挙動が変だ。

では、お後がよろしいようで。
2020-04-29 太田